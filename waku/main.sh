

sudo apt update && sudo apt upgrade -y
sudo apt install gcc ca-certificates curl gnupg unzip lsb-release git jq make curl build-essential lz4 -y
# Create docker keyring
    sudo install -m 0755 -d /etc/apt/keyrings
    sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
    sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add docker repo 
    echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
    $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install docker
    sudo apt-get update && sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    systemctl start docker
    # Clone the Allora Chain repository
    if ! git clone https://github.com/allora-network/allora-chain.git; then
        echo "Failed to clone Allora Chain repository. Exiting."
        sleep 13
        mainmenu
        return 1
    fi

git clone https://github.com/waku-org/nwaku-compose && cd nwaku-compose
cp .env.example .env



# Enter variables
echo -e "\e[4;33m🧭 Enter your client adress: \e[0m" && echo
read -p RLN_RELAY_ETH_CLIENT_ADDRESS
echo
echo -e "\e[4;33m🔑 Enter your eth private key: \e[0m" && echo
read -p ETH_TESTNET_KEY
echo
echo -e "\e[4;33m🛡️Enter your password: \e[0m" && echo
read -p RLN_RELAY_CRED_PASSWORD

# Change values in the file nwaku-compose/.env
sed -i "s|^RLN_RELAY_ETH_CLIENT_ADDRESS=.*|RLN_RELAY_ETH_CLIENT_ADDRESS=$RLN_RELAY_ETH_CLIENT_ADDRESS|" nwaku-compose/.env
sed -i "s|^ETH_TESTNET_KEY=.*|ETH_TESTNET_KEY=$ETH_TESTNET_KEY|" nwaku-compose/.env
sed -i "s|^RLN_RELAY_CRED_PASSWORD=.*|RLN_RELAY_CRED_PASSWORD=\"$RLN_RELAY_CRED_PASSWORD\"|" nwaku-compose/.env