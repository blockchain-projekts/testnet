#!/bin/bash
clear && echo -e "\e[32m
╦╔╗╔╔═╗╔╦╗╔═╗╦  ╦  ╔═╗╔╦╗╦╔═╗╔╗╔            
║║║║╚═╗ ║ ╠═╣║  ║  ╠═╣ ║ ║║ ║║║║            
╩╝╚╝╚═╝ ╩ ╩ ╩╩═╝╩═╝╩ ╩ ╩ ╩╚═╝╝╚╝ \e[0m"
mainmenu() {
	echo
	echo -e "\e[29mMinimum hardware requirements.\e[0m" && echo
	echo -e "\e[1;4;29m| 4 CPU | 8 RAM | 200 GB |\e[0m"
	echo
	echo -e "\e[29mRecommended hardware requirements.\e[0m" && echo
	echo -e "\e[1;4;29m| 8 CPU | 16 RAM | 400 GB |\e[0m"
	echo
	echo -e "\e[4;33mDo you want to start the installation?\e[0m" && echo
	echo -e "1. \e[2;31mYes\e[0m"
	echo -e "2. \e[1;32mNo\e[0m"
	echo
    echo -ne "\e[4;34mYour selection:\e[0m "
    read ans
	case $ans in
		1) yes ;;
		2) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/main.sh) ;;
		*) clear && echo "Invalid request!" && mainmenu ;;
    esac
}

yes(){
	echo
	sudo apt update > /dev/null 2>&1 &&sudo apt install curl screen npm -y
	sudo apt install docker.io -y
	docker --version

	sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose
	docker-compose --version

	curl -O https://gitlab.com/shardeum/validator/dashboard/-/raw/main/installer.sh && chmod +x installer.sh && ./installer.sh

	docker update --restart always shardeum-dashboard && docker start shardeum-dashboard && docker exec -i shardeum-dashboard /bin/bash -c "operator-cli gui start && operator-cli start" && curl -sS -o autostart_shardeum.sh https://raw.githubusercontent.com/dzhagerr/xl1/main/xscript/nodes/active/shardeum/autostart_shardeum.sh && chmod +x autostart_shardeum.sh && screen -dmS autostart_shardeum bash autostart_shardeum.sh
	docker exec -i shardeum-dashboard /bin/bash -c "pm2 list"
	screen -ls
	subnmenu

}

subnmenu() {
	echo -ne "
	$(printCyan	'Installation completed') $(printCyanBlink '!!!')
	$(printCyan	'Press Enter:') "
	read -r ans
	case $ans in
		*) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/control.sh) ;;
    esac
}

mainmenu



sudo apt update && sudo apt upgrade -y
sudo apt install gcc ca-certificates curl gnupg unzip lsb-release git jq make curl build-essential lz4 -y
# Create docker keyring
    sudo install -m 0755 -d /etc/apt/keyrings
    sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
    sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add docker repo 
    echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
    $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install docker
    sudo apt-get update && sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    systemctl start docker
    # Clone the Allora Chain repository
    if ! git clone https://github.com/allora-network/allora-chain.git; then
        echo "Failed to clone Allora Chain repository. Exiting."
        sleep 13
        mainmenu
        return 1
    fi

git clone https://github.com/waku-org/nwaku-compose && cd nwaku-compose
cp .env.example .env



# Enter variables
echo -e "\e[4;33m🧭 Enter your client adress: \e[0m" && echo
read -p RLN_RELAY_ETH_CLIENT_ADDRESS
echo
echo -e "\e[4;33m🔑 Enter your eth private key: \e[0m" && echo
read -p ETH_TESTNET_KEY
echo
echo -e "\e[4;33m🛡️Enter your password: \e[0m" && echo
read -p RLN_RELAY_CRED_PASSWORD

# Change values in the file nwaku-compose/.env
sed -i "s|^RLN_RELAY_ETH_CLIENT_ADDRESS=.*|RLN_RELAY_ETH_CLIENT_ADDRESS=$RLN_RELAY_ETH_CLIENT_ADDRESS|" nwaku-compose/.env
sed -i "s|^ETH_TESTNET_KEY=.*|ETH_TESTNET_KEY=$ETH_TESTNET_KEY|" nwaku-compose/.env
sed -i "s|^RLN_RELAY_CRED_PASSWORD=.*|RLN_RELAY_CRED_PASSWORD=\"$RLN_RELAY_CRED_PASSWORD\"|" nwaku-compose/.env

#  register node
./register_rln.sh

# Run node
docker-compose up -d

# Done installation
complete_installation() {
	echo -e "\e[5;1;32mInstallation complete! \e[0m"
	echo
	echo -e "\e[3;33mPlease go to website gaianet and connect your node. \e[0m"
	echo -e "\e[3;3mhttps://www.gaianet.ai/ \e[0m"
    echo
    echo -ne "\e[4;34mPress Enter to go main menu. \e[0m"
    read -r ans
    case $ans in
        *) cd && source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/waku/main.sh) ;;
    esac
}