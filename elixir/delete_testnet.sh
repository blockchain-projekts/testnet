#!/bin/bash
clear && echo -e "\e[31m
╔╦╗╔═╗╦  ╔═╗╔╦╗╔═╗                          
 ║║║╣ ║  ║╣  ║ ║╣                           
═╩╝╚═╝╩═╝╚═╝ ╩ ╚═╝\e[0m"
mainmenu() {
	echo
	echo -e "\e[4;33mYou sure you want to delete Elixir?\e[0m" && echo
	echo -e "1. \e[2;31mYes\e[0m"
	echo -e "2. \e[1;32mNo\e[0m"
	echo
    echo -ne "\e[4;34mYour selection:\e[0m "
    read ans
	case $ans in
		1) yes ;;
		2) no ;;
		*) echo "Invalid request!" && mainmenu
		;;
	esac
}

no(){
	source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/elixir/main.sh)
}

yes(){
echo -ne "\e[33mDeleting.....!\e[0m"
cd $HOME
docker kill elixir-testnet
docker rm elixir-testnet
rm -rf $HOME/elixir-metadata
submenu
}


submenu(){
    echo -e "\e[32m
┌─┐┌─┐┌┬┐┌─┐┬  ┌─┐┌┬┐┌─┐┌┬┐                 
│  │ ││││├─┘│  ├┤  │ ├┤  ││                 
└─┘└─┘┴ ┴┴  ┴─┘└─┘ ┴ └─┘─┴┘\e[0m" 
echo -e "\e[2;32mElixir deleted successfully!\e[0m"
echo
echo -n "Press Enter:  "
	read -r ans
	case $ans in
		*) clear && source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/elixir/main.sh) ;;
	esac
}
mainmenu