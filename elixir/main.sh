#!/bin/bash
clear && echo -e "\e[35m
╔═╗╦  ╦═╗ ╦╦╦═╗                             
║╣ ║  ║╔╩╦╝║╠╦╝                             
╚═╝╩═╝╩╩ ╚═╩╩╚═  \e[0m"
mainmenu() {
    echo "    ------Mainnet------ "
    echo " 📖 | 1.View logs (Mainnet)"
    echo " 💿 | 2.Install Elixir Mainnet"
    echo " 🔄 | 3.Update Elixir Mainnet"
    echo "    ------Testnet------ "
    echo " 📖 | 4.View logs (Testnet)"
    echo " 💿 | 5.Install Elixir Testnet"
    echo " 🔄 | 6.Update Elixir Testnet"
    echo "    ---------- "
    echo " 🔄 | 7.Convert node to mainnet"
    echo " 🗑️  | 8.Delete Mainnet node"
    echo " 🗑️  | 9.Delete Testnet node"
    echo "  ----------------"
    echo "  0.Back | 10.Exit"
    echo "  ----------------"
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
        
        1) logs_mainnet ;;
        2) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/elixir/install_mainnet.sh) ;;
        3) update_mainnet ;;
        4) logs_testnet ;;
        5) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/elixir/install_testnet.sh) ;;
        6) update_testnet ;;
        7) convert ;;
        8) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/elixir/delete_mainnet.sh) ;;
        9) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/elixir/delete_testnet.sh) ;;
        0) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/testnet.sh) ;;
        10) echo "Goodbye." && exit ;;
        *) echo -e "\e[31mInvalid request! \e[0m" && mainmenu ;;
    esac
}


logs_testnet(){
    docker logs -f --tail 1000 elixir-testnet 
    mainmenu
}

logs_mainnet(){
    docker logs -f --tail 1000 elixir 
    mainmenu
}

update_testnet(){
    docker kill elixir-testnet
    docker rm elixir-testnet
    docker pull elixirprotocol/validator:testnet --platform linux/amd64
    docker run -d --env-file $HOME/elixir-metadata/validator-testnet.env --platform linux/amd64 --name elixir-testnet -p 17691:17691 elixirprotocol/validator:testnet
    echo -e "\e[32mElixir node updated successfully! \e[0m"
    mainmenu
}

update_mainnet(){
    docker kill elixir
    docker rm elixir
    docker pull elixirprotocol/validator --platform linux/amd64
    docker run --env-file $HOME/elixir-metadata/validator.env --platform linux/amd64 --name elixir -p 17690:17690 elixirprotocol/validator
    echo -e "\e[32mElixir node updated successfully! \e[0m"
    mainmenu
}

convert(){
    docker kill elixir
    docker rm elixir
    sed -i "s/^ENV=.*/ENV=prod/" $HOME/elixir-metadata/validator.env
    docker pull elixirprotocol/validator --platform linux/amd64
    docker run --env-file $HOME/elixir-metadata/validator.env --platform linux/amd64 --name elixir -p 17690:17690 elixirprotocol/validator
    echo -e "\e[32mNode converted to mainnet successfully! \e[0m"
    mainmenu
}

mainmenu