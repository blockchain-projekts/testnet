#!/bin/bash

clear && echo -e "\e[32m
╦╔╗╔╔═╗╔╦╗╔═╗╦  ╦  ╔═╗╔╦╗╦╔═╗╔╗╔            
║║║║╚═╗ ║ ╠═╣║  ║  ╠═╣ ║ ║║ ║║║║            
╩╝╚╝╚═╝ ╩ ╩ ╩╩═╝╩═╝╩ ╩ ╩ ╩╚═╝╝╚╝ \e[0m"

mainmenu() {
    echo
    echo -e "\e[29mRecommended hardware requirements.\e[0m" && echo
    echo -e "\e[1;4;29m| 4 CPU | 8 RAM | 100 GB |\e[0m"
    echo 
	echo -e "\e[4;33mDo you want to start the installation?\e[0m" && echo
	echo -e "🟢 | 1.\e[1;32mYes\e[0m"
	echo -e "🔴 | 2.\e[1;31mNo\e[0m"
    echo
    echo -ne "\e[4;34mYour selection:\e[0m "
    read ans
    case $ans in
        1) yes ;;
        2) no ;;
        *) clear && echo "Invalid request!" && mainmenu ;;
    esac
}

no(){
    echo "Installation aborted."
    source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/elixir/main.sh)
}

yes(){
    sudo apt update > /dev/null 2>&1
    sudo apt install curl -y

    # Check if Docker is installed
    if ! command -v docker &> /dev/null
    then
        echo "Docker not found. Installing Docker..."
        sudo install -m 0755 -d /etc/apt/keyrings
        sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
        sudo chmod a+r /etc/apt/keyrings/docker.asc

        echo \
        "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
        $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
        sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

        sudo apt-get update && sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
        sudo systemctl start docker
        sudo systemctl enable docker
    else
        echo "Docker is already installed."
    fi

docker pull elixirprotocol/validator:v3
git clone https://github.com/fmsuicmc/elixir-metadata

    read -r -p "Enter validator name: " VALIDATOR_NAME
    read -r -p "Enter your Metamask address: " METAMASK_ADDRESS
    read -r -p "Enter your Metamask private key: " PRIVATE_KEY_ELIXIR

    IP=$(curl ifconfig.me)

# Add environment variables to validator.env using sed
    sed -i "s/^STRATEGY_EXECUTOR_IP_ADDRESS=.*/STRATEGY_EXECUTOR_IP_ADDRESS=$IP/" elixir-metadata/validator.env
    sed -i "s/^STRATEGY_EXECUTOR_DISPLAY_NAME=.*/STRATEGY_EXECUTOR_DISPLAY_NAME=$VALIDATOR_NAME/" elixir-metadata/validator.env
    sed -i "s/^STRATEGY_EXECUTOR_BENEFICIARY=.*/STRATEGY_EXECUTOR_BENEFICIARY=$METAMASK_ADDRESS/" elixir-metadata/validator.env
    sed -i "s/^SIGNER_PRIVATE_KEY=.*/SIGNER_PRIVATE_KEY=$PRIVATE_KEY_ELIXIR/" elixir-metadata/validator.env
    echo -e "\e[32mValidator setup complete!\e[0m"
    docker run -d --env-file $HOME/elixir-metadata/validator.env --platform linux/amd64 -p 17690:17690 --name elixir --restart unless-stopped elixirprotocol/validator:v3

    echo -e "\e[32mValidator started!\e[0m"
    submenu
}

submenu(){
    echo -ne "
    \e[32mINSTALLATION COMPLETED........ \e[0m \e[32m!!! \e[0m
    
    \e[36mPress Enter: \e[0m"
    read -r ans
    case $ans in
        *) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/elixir/main.sh) ;;
    esac
}

mainmenu