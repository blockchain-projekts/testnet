#!/bin/bash
clear && echo -e "\e[32m
╦╔╗╔╔═╗╔╦╗╔═╗╦  ╦  ╔═╗╔╦╗╦╔═╗╔╗╔            
║║║║╚═╗ ║ ╠═╣║  ║  ╠═╣ ║ ║║ ║║║║            
╩╝╚╝╚═╝ ╩ ╩ ╩╩═╝╩═╝╩ ╩ ╩ ╩╚═╝╝╚╝ \e[0m"
mainmenu() {
	echo
	echo -e "\e[29mRecommended hardware requirements.\e[0m" && echo
	echo -e "\e[1;4;29m| 4 CPU | 8 RAM | 500 GB |\e[0m"
	echo
	echo -e "\e[4;33mDo you want to start the installation?\e[0m" && echo
	echo -e "1. \e[2;31mYes\e[0m"
	echo -e "2. \e[1;32mNo\e[0m"
	echo
    echo -ne "\e[4;34mYour selection:\e[0m "
    read ans
	case $ans in
		1) yes ;;
		2) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/story_protocol/main.sh) ;;
		*) clear && echo "Invalid request!" && mainmenu ;;
    esac
}

yes(){
	echo
    sudo apt update && apt upgrade -y
	sudo apt install curl git jq lz4 build-essential ufw basez -y

read -r -p "  Input your moniker:  " MONIKER

#install go
sudo rm -rf /usr/local/go
curl -Ls https://go.dev/dl/go1.22.8.linux-amd64.tar.gz | sudo tar -xzf - -C /usr/local
eval $(echo 'export PATH=$PATH:/usr/local/go/bin' | sudo tee /etc/profile.d/golang.sh)
eval $(echo 'export PATH=$PATH:$HOME/go/bin' | tee -a $HOME/.profile)

# Clone project repository
cd $HOME
rm -rf story
git clone https://github.com/piplabs/story.git
cd story
git checkout v0.12.1


# Build binaries
go build -o story ./client

# Prepare binaries for Cosmovisor
mkdir -p $HOME/.story/story/cosmovisor/genesis/bin
mv story $HOME/.story/story/cosmovisor/genesis/bin/

# Create application symlinks
sudo ln -s $HOME/.story/story/cosmovisor/genesis $HOME/.story/story/cosmovisor/current -f
sudo ln -s $HOME/.story/story/cosmovisor/current/bin/story /usr/local/bin/story -f

# Clone project repository
cd $HOME
rm -rf story-geth
git clone https://github.com/piplabs/story-geth.git
cd story-geth
git checkout v0.10.1
# Build binaries
make geth
sudo mv build/bin/geth /usr/local/bin/

# Download and install Cosmovisor
go install cosmossdk.io/tools/cosmovisor/cmd/cosmovisor@v1.7.0

# Create service
sudo tee /etc/systemd/system/story-testnet.service > /dev/null << EOF
[Unit]
Description=story node service
After=network-online.target

[Service]
User=$USER
ExecStart=$(which cosmovisor) run run
Restart=on-failure
RestartSec=10
LimitNOFILE=65535
Environment="DAEMON_HOME=$HOME/.story/story"
Environment="DAEMON_NAME=story"
Environment="UNSAFE_SKIP_BACKUP=true"
Environment="PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:$HOME/.story/story/cosmovisor/current/bin"

[Install]
WantedBy=multi-user.target
EOF
sudo systemctl daemon-reload
sudo systemctl enable story-testnet.service

sudo tee /etc/systemd/system/story-testnet-geth.service > /dev/null << EOF
[Unit]
Description=Story Execution Client service
After=network-online.target

[Service]
User=$USER
WorkingDirectory=~
ExecStart=/usr/local/bin/geth --iliad --syncmode full --http --ws
Restart=on-failure
RestartSec=10
LimitNOFILE=65535

[Install]
WantedBy=multi-user.target
EOF
sudo systemctl daemon-reload
sudo systemctl enable story-testnet-geth.service

# Initialize the node
story init --moniker $MONIKER --network iliad

# Add seeds
sed -i -e "s|^seeds *=.*|seeds = \"3f472746f46493309650e5a033076689996c8881@story-testnet.rpc.kjnodes.com:26659\"|" $HOME/.story/story/config/config.toml

# Make geth directory
mkdir -p $HOME/.story/geth

curl -L https://snapshots.kjnodes.com/story-testnet/snapshot_latest_geth.tar.lz4 | tar -Ilz4 -xf - -C $HOME/.story/geth
curl -L https://snapshots.kjnodes.com/story-testnet/snapshot_latest.tar.lz4 | tar -Ilz4 -xf - -C $HOME/.story/story

sudo systemctl start story-testnet-geth.service
sudo systemctl start story-testnet.service
cd
subnmenu

}

subnmenu() {
	echo -ne "
	Installation completed
	Press Enter: "
	read -r ans
	case $ans in
		*) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/story_protocol/main.sh) ;;
    esac
}

mainmenu