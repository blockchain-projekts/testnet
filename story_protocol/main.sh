#!/bin/bash
clear && echo -e "\e[36m
╔═╗┌┬┐┌─┐┬─┐┬ ┬  ╔═╗┬─┐┌─┐┌┬┐┌─┐┌─┐┌─┐┬  
╚═╗ │ │ │├┬┘└┬┘  ╠═╝├┬┘│ │ │ │ ││  │ ││  
╚═╝ ┴ └─┘┴└─ ┴   ╩  ┴└─└─┘ ┴ └─┘└─┘└─┘┴─┘ \e[0m"
mainmenu() {
    echo    
    echo " 🖥️  | 1. Check status sync"
    echo " 📖 | 2. Execution Client logs"
    echo " 📖 | 3. Consensus Client logs"
    echo " 💿 | 4. Install"
    echo " 📝 | 5. Validator HEX Address"
    echo " ----------------"
    echo "  0.Back | 10.Exit"
    echo " ----------------"
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
        1) curl -s http://localhost:26657/status | jq .result.sync_info && mainmenu ;;
        2) journalctl -fu story-testnet-geth.service -o cat && mainmenu ;;
        3) journalctl -fu story-testnet.service -o cat && mainmenu ;;
        4) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/story_protocol/install.sh) ;;
        5) hex ;;
        0) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/testnet.sh) ;;
        10) echo "Goodbye." && exit ;;
        *) echo "Invalid request!" && mainmenu ;;
    esac
}
mainmenu


hex() {
    echo
    curl -s http://localhost:26657/status | jq -r .result.validator_info.address
    echo
    echo "https://tools.synergynodes.com"
    echo
    mainmenu
}