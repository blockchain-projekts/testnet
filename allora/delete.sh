#!/bin/bash
clear && echo -e "\e[31m
╔╦╗╔═╗╦  ╔═╗╔╦╗╔═╗                          
 ║║║╣ ║  ║╣  ║ ║╣                           
═╩╝╚═╝╩═╝╚═╝ ╩ ╚═╝\e[0m"
mainmenu() {
    echo
    echo -e "\e[4;33mDo you want to delete the Allora Node and Prediction Node?\e[0m" && echo
    echo -e "🟢 | 1.\e[2;32mYes\e[0m"
    echo -e "🔴 | 2.\e[1;31mNo\e[0m"
    echo
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
        1) yes ;;
        2) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/allora/main.sh) ;;
        *) clear && echo "Invalid request!" && mainmenu ;;
    esac
}

yes() {
    echo -e "\e[31mUninstalling Allora Node and Prediction Node...\e[0m"
    cd $HOME/basic-coin-prediction-node && docker compose down && cd
    
    # Remove docker images
    echo -e "\e[34mRemoving docker images...\e[0m"
    if docker rmi alloranetwork/allora-inference-base:latest; then
        echo -e "\e[32mDocker images removed successfully.\e[0m"
    else
        echo -e "\e[31mFailed to remove Docker images.\e[0m"
    fi

    # Remove directories
    echo -e "\e[34mRemoving directories...\e[0m"
    if [ -d "$HOME/basic-coin-prediction-node" ]; then
        rm -rf $HOME/basic-coin-prediction-node
    fi
    if [ -d "$HOME/allora-chain" ]; then
        rm -rf $HOME/allora-chain
    fi
    echo -e "\e[32mDirectories removed successfully.\e[0m"

    # Remove allorad keys
    echo -e "\e[34mRemoving allorad keys...\e[0m"
    if [ -d "$HOME/.allorad" ]; then
        rm -rf $HOME/.allorad
        echo -e "\e[32mallorad keys removed successfully.\e[0m"
    else
        echo -e "\e[31mFailed to remove allorad keys.\e[0m"
    fi

    complete_uninstallation
}

complete_uninstallation() {
    echo -e "\e[32m
┌─┐┌─┐┌┬┐┌─┐┬  ┌─┐┌┬┐┌─┐┌┬┐                 
│  │ ││││├─┘│  ├┤  │ ├┤  ││                 
└─┘└─┘┴ ┴┴  ┴─┘└─┘ ┴ └─┘─┴┘\e[0m" 
echo -e "\e[2;32mAllora deleted successfully!\e[0m"
echo
echo -n "Press Enter:  "
    case $ans in
        *) cd && source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/allora/main.sh) ;;
    esac
}

mainmenu