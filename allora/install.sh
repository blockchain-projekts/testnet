#!/bin/bash
clear && echo -e "\e[32m
╦╔╗╔╔═╗╔╦╗╔═╗╦  ╦  ╔═╗╔╦╗╦╔═╗╔╗╔            
║║║║╚═╗ ║ ╠═╣║  ║  ╠═╣ ║ ║║ ║║║║            
╩╝╚╝╚═╝ ╩ ╩ ╩╩═╝╩═╝╩ ╩ ╩ ╩╚═╝╝╚╝\e[0m"
mainmenu() {
    echo
	echo -e "\e[29mRecommended hardware requirements.\e[0m" && echo
	echo -e "\e[1;4;29m| 2 CPU | 4 RAM | 5 GB |\e[0m"
	echo 
	echo -e "\e[4;33mDo you want to start the installation?\e[0m" && echo
	echo -e "🟢 | 1.\e[1;32mYes\e[0m"
	echo -e "🔴 | 2.\e[1;31mNo\e[0m"
	echo
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
	case $ans in
		1) yes ;;
		2) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/allora/main.sh) ;;
		*) clear && echo "Invalid request!" && mainmenu ;;
    esac
}

yes() {
# Update and upgrade
	sudo apt-get update && sudo apt-get upgrade -y
# Install dependencies
	sudo apt-get install -y make build-essential unzip lz4 gcc git jq ncdu tmux cmake clang pkg-config libssl-dev python3-pip protobuf-compiler bc
# Install Go
	GO_VERSION="1.22.5"
	wget "https://golang.org/dl/go${GO_VERSION}.linux-amd64.tar.gz"
	sudo tar -C /usr/local -xzf "go${GO_VERSION}.linux-amd64.tar.gz"
	rm "go${GO_VERSION}.linux-amd64.tar.gz"
	echo "export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin" >> $HOME/.bash_profile
	source $HOME/.bash_profile
	go version
# Install docker
# Create docker keyring
    sudo install -m 0755 -d /etc/apt/keyrings
    sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
    sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add docker repo 
    echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
    $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install docker
    sudo apt-get update && sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    systemctl start docker
    
# Clone the Allora Chain repository
    if ! git clone https://github.com/allora-network/allora-chain.git; then
        echo "Failed to clone Allora Chain repository. Exiting."
        sleep 13
        mainmenu
        return 1
    fi
# Build the Allora Chain
    cd allora-chain && make all


    if ! command -v allorad &> /dev/null; then
        echo "allorad not found in PATH. Adding it..."
        export PATH=$PATH:$(pwd)/build
        echo "export PATH=$PATH:$(pwd)/build" >> $HOME/.bash_profile
        source $HOME/.bash_profile
    fi

    create_wallet
    complete_installation
}

# Create a new wallet or restore an existing one
create_wallet() {
	echo
    echo -e "\e[4;34mYou want to create a new wallet allora or restore an existing one?\e[0m"
	echo
    echo -e "1. Create a new wallet"
    echo -e "2. Restore an existing wallet"
    echo
	echo -ne "\e[4;34mYour choice: \e[0m"
    read -r wallet_choice
    case $wallet_choice in
        1)
            echo -e "\e[4;33mCreating a new wallet...\e[0m"
            allorad keys add wallet
			echo
            echo -e "\e[1;4;31mWRITE THE ADDRESS AND MNEMONIC!!! \e[0m"
			echo
			echo -ne "\e[4;34mPress Enter to continue. \e[0m"
            read -r 
            ;;
        2)
            echo -e "\e[4;33mRestoring an existing wallet... \e[0m"
            allorad keys add wallet --recover
            ;;
        *)
            echo "Invalid choice."
			create_wallet
            ;;
    esac
}

# Done installation
complete_installation() {
	echo -e "\e[5;1;32mInstallation complete! \e[0m"
	echo
	echo -e "\e[3;33mPlease faucet token to your wallet. \e[0m"
	echo -e "\e[3;33mhttps://faucet.testnet-1.testnet.allora.network/ \e[0m"
    echo
    echo -ne "\e[4;34mPress Enter to go main menu. \e[0m"
    read -r ans
    case $ans in
        *) cd && source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/allora/main.sh) ;;
    esac
}
mainmenu