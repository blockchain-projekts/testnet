#!/bin/bash
clear && echo -e "\e[32m
╦╔╗╔╔═╗╔╦╗╔═╗╦  ╦  ╔═╗╔╦╗╦╔═╗╔╗╔            
║║║║╚═╗ ║ ╠═╣║  ║  ╠═╣ ║ ║║ ║║║║            
╩╝╚╝╚═╝ ╩ ╩ ╩╩═╝╩═╝╩ ╩ ╩ ╩╚═╝╝╚╝\e[0m" && echo
mainmenu() {
    echo
	echo -e "\e[4;33mDo you want to start the installation?\e[0m" && echo
	echo -e "🟢 | 1.\e[1;32mYes\e[0m"
	echo -e "🔴 | 2.\e[1;31mNo\e[0m"
	echo
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
	case $ans in
		1) yes ;;
		2) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/allora/main.sh) ;;
		*) clear && echo "Invalid request!" && mainmenu ;;
    esac
}

yes() {
    # Check if the directory exists and is not empty
    if [ -d "$HOME/basic-coin-prediction-node" ] && [ "$(ls -A $HOME/basic-coin-prediction-node)" ]; then
        echo "Directory $HOME/basic-coin-prediction-node already exists and is not empty. Removing it."
        rm -rf $HOME/basic-coin-prediction-node
    fi

    if ! git clone https://github.com/allora-network/basic-coin-prediction-node; then
        echo "Failed to clone repository. Returning to main menu."
        sleep 13
        mainmenu
        return 1
    fi
    mkdir -p $HOME/basic-coin-prediction-node/worker-data $HOME/basic-coin-prediction-node/head-data && 
    sudo chmod -R 777 $HOME/basic-coin-prediction-node/worker-data $HOME/basic-coin-prediction-node/head-data && 
    cd $HOME/basic-coin-prediction-node || { echo "Failed to change directory. Returning to main menu."; sleep 3; mainmenu; return 1; }

    if ! sudo docker run -it --entrypoint=bash -v $(pwd)/head-data:/data alloranetwork/allora-inference-base:latest -c "mkdir -p /data/keys && (cd /data/keys && allora-keys)"; then
        echo -e "\e[1;31mFailed to run docker for head-data. Returning to main menu.\e[0m"
        sleep 13
        mainmenu
        return 1
    fi

    if ! sudo docker run -it --entrypoint=bash -v $(pwd)/worker-data:/data alloranetwork/allora-inference-base:latest -c "mkdir -p /data/keys && (cd /data/keys && allora-keys)"; then
        echo -e "\e[1;31mFailed to run docker for worker-data. Returning to main menu.\e[0m"
        sleep 13
        mainmenu
        return 1
    fi
			echo
            echo -e "\e[1;5;31mWRITE THE IDENTITY!!! \e[0m"
			echo
            cat head-data/keys/identity
            echo
            echo -e "\e[4;34mPress Enter to continue \e[0m"
			read -r 

    rm -rf docker-compose.yml
    # Write ALLORA_HEAD_ID
    echo -e "\e[34mEnter IDENTITY, which you copied earlier:\e[0m"
    read -r allora_head_id

    # Write your wallet mnemonic
    echo -e "\e[34mEnter your wallet mnemonic:\e[0m"
    read -r allora_mnemonic

    # Download docker-compose.yml
    if ! wget https://raw.githubusercontent.com/kulikovae/allora/main/docker-compose.yml; then
        echo -e "\e[1;31mFailed to download docker-compose.yml. Returning to main menu.\e[0m"
        sleep 13
        mainmenu
        return 1
    fi

    # Replace ALLORA_HEAD_ID and ALLORA_MNEMONIC
    sed -i "s/ALLORA_HEAD_ID/$allora_head_id/g" docker-compose.yml
    sed -i "s/ALLORA_MNEMONIC/$allora_mnemonic/g" docker-compose.yml

    # Build and run the docker compose
    if docker compose build && docker compose up -d; then
        # Done installation
        echo -e "\e[5;1;32mInstallation complete! \e[0m"
        echo
        echo -e "\e[4;34mPress Enter to go main menu or 1 to view logs. \e[0m"
        read -r ans
        case $ans in
            1) (cd $HOME/basic-coin-prediction-node && docker compose logs -f) ;;
            *) cd && source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/allora/main.sh) ;;
        esac
    else
        echo -e "\e[1;31mFailed to build or run docker compose. Returning to main menu.\e[0m"
        sleep 13    
        mainmenu
        return 1
    fi
}

mainmenu