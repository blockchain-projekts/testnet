#!/bin/bash
clear && echo -e "\e[34m
╔═╗╦  ╦  ╔═╗╦═╗╔═╗                          
╠═╣║  ║  ║ ║╠╦╝╠═╣                          
╩ ╩╩═╝╩═╝╚═╝╩╚═╩ ╩\e[0m" 
mainmenu() {
    echo
    echo " 💰 | 1.Check balance wallet"
    echo " 🔋 | 2.Check status node"
    echo " 📖 | 3.View logs"
    echo " 💿 | 4.Install Allora node"
    echo " 💿 | 5.Install price prediction node"
    echo " 🗑️  | 6.Delete node"
    echo " 📚 | 7.Guide"
    echo "  ----------------"
    echo "  0.Back | 10.Exit"
    echo "  ----------------"
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
        1) echo && allorad query bank balances wallet && echo && mainmenu ;;
        2) echo "--------------------------" && status-node ;;
        3) (cd $HOME/basic-coin-prediction-node && docker compose logs -f) && mainmenu ;;
        4) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/allora/install.sh) ;;
        5) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/allora/prediction_node.sh) ;;
        6) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/allora/delete.sh) ;;
        7) guide ;;
        0) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/testnet.sh) ;;
        10) echo "Goodbye." && exit ;;
        *) echo "Invalid request!" && mainmenu ;;
    esac
}

status-node() {
      curl --location 'http://localhost:6000/api/v1/functions/execute' \
--header 'Content-Type: application/json' \
--data '{
    "function_id": "bafybeigpiwl3o73zvvl6dxdqu7zqcub5mhg65jiky2xqb4rdhfmikswzqm",
    "method": "allora-inference-function.wasm",
    "parameters": null,
    "topic": "1",
    "config": {
        "env_vars": [
            {
                "name": "BLS_REQUEST_PATH",
                "value": "/api"
            },
            {
                "name": "ALLORA_ARG_PARAMS",
                "value": "ETH"
            }
        ],
        "number_of_nodes": -1,
        "timeout": 2
    }
}'
mainmenu
}


view_logs() {
    cd $HOME/allora-chain || { echo "Failed to change directory to $HOME/allora-chain. Returning to main menu."; mainmenu; return 1; }
    docker compose logs
    echo -e "\e[4;34mPress Enter to go back to main menu.\e[0m"
    read -r
    mainmenu
}

guide() {
clear && echo -e "\e[35m
╔═╗╦ ╦╦╔╦╗╔═╗  
║ ╦║ ║║ ║║║╣   
╚═╝╚═╝╩═╩╝╚═╝ \e[0m" 
    echo
    echo -ne "\e[3;33mAllora (formerly known as Upshot) operates as a decentralized AI network, 
focusing on enhancing blockchain capabilities with smarter and more secure artificial 
intelligence. It supports scalable AI solutions with guaranteed privacy, enabling 
applications in various industries to leverage advanced predictive analytics and automation.\e[0m"
    echo && echo
    echo -e "\e[4;32mEnglish guide:\e[0m"
    echo "Medium: Comming soon"
    echo
    echo -e "\e[4;32mRussian guide:\e[0m"
    echo "Boosty: Comming soon"
    echo
    echo -e "\e[4;34mPress Enter to go back to main menu.\e[0m "
    read -r
    mainmenu
}

mainmenu