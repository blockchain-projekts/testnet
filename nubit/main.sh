#!/bin/bash
clear && echo -e "\e[33m
╔╗╔╦ ╦╔╗ ╦╔╦╗                               
║║║║ ║╠╩╗║ ║                                
╝╚╝╚═╝╚═╝╩ ╩ \e[0m"
mainmenu() {
    echo    
    echo " 🖥️  | 1. Check node status"
    echo " 📖 | 2. View logs"
    echo " 🔄 | 3. Restart node"
    echo " 💿 | 4. Install"
    echo " 🗑️  | 5. Delete"
    echo "  ----------------"
    echo "  0.Back | 10.Exit"
    echo "  ----------------"
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
        1) lsof -i :2121 | grep LISTEN && mainmenu ;;
        2) tail -f $HOME/nubit-light.log && mainmenu ;;
        3) nohup bash <(curl -s https://nubit.sh) > $HOME/nubit-light.log 2>&1 && mainmenu ;;
        4) apt install -y lsof && nohup bash <(curl -s https://nubit.sh) > $HOME/nubit-light.log 2>&1 && mainmenu ;;
        5) kill -9 $(lsof -t -i:2121) && mainmenu ;;
        0) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/testnet.sh) ;;
        10) echo "Goodbye." && exit ;;
        *) echo "Invalid request!" && mainmenu ;;
    esac
}
mainmenu
