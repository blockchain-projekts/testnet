#!/bin/bash
clear && echo -e "\e[34m
╔═╗╦  ╦╔═╗╔╗╔╔═╗╔╦╗
╠═╣║  ║║ ╦║║║║╣  ║║
╩ ╩╩═╝╩╚═╝╝╚╝╚═╝═╩╝\e[0m" 
mainmenu() {
    echo
    echo " 💿 | 1.Install Proof"
    echo " 🗑️  | 2.Delete node"
    echo "  ----------------"
    echo "  0.Back | 10.Exit"
    echo "  ----------------"
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
        1) install ;;
        2) delete ;;
        0) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/testnet.sh) ;;
        10) echo "Goodbye." && exit ;;
        *) echo "Invalid request!" && mainmenu ;;
    esac
}


install() {
curl -L https://raw.githubusercontent.com/yetanotherco/aligned_layer/main/batcher/aligned/install_aligned.sh | bash
source ./.bashrc
curl -L https://raw.githubusercontent.com/yetanotherco/aligned_layer/main/batcher/aligned/get_proof_test_files.sh | bash
rm -rf ~/.aligned/aligned_verification_data/ &&
aligned submit \
--proving_system SP1 \
--proof ~/.aligned/test_files/sp1_fibonacci.proof \
--vm_program ~/.aligned/test_files/sp1_fibonacci.elf \
--aligned_verification_data_path ~/.aligned/aligned_verification_data \
--conn wss://batcher.alignedlayer.com 
echo -e "\e[33mClick url to check transaction and twitter proof ☝️\e[0m"
echo -e "\e[32mProof submitted\e[0m"
echo -e "\e[33mEnter to go main menu\e[0m"
read -r
mainmenu
}

delete() {
    rm -rf .aligned
    echo -e "\e[32mNode deleted\e[0m"
    echo
    echo -e "\e[33mEnter to go main menu\e[0m"
    read -r
    mainmenu
}
mainmenu