 
#!/bin/bash
clear && echo -e "\e[35m
╔═╗╔═╗╦╔═╗╔╗╔╔═╗╔╦╗     
║ ╦╠═╣║╠═╣║║║║╣  ║      
╚═╝╩ ╩╩╩ ╩╝╚╝╚═╝ ╩\e[0m" 
mainmenu() {
    echo
    echo " 👁️‍🗨️ | 1.View node identity"
    echo " 💿 | 2.Install Gaia node"
    echo " 🔄 | 3.Upgrade node"
    echo " 🛑 | 4.Stop node"
    echo " 🗑️  | 5.Delete node"
    echo " 📚 | 6.Guide"
    echo "  ----------------"
    echo "  0.Back | 10.Exit"
    echo "  ----------------"
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
        1) echo && gaianet info && echo && mainmenu ;;
        2) install ;;
        3) update ;;
        4) stop ;;
        5) delete ;;
        6) guide ;;
        0) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/testnet.sh) ;;
        10) echo "Goodbye." && exit ;;
        *) echo "Invalid request!" && mainmenu ;;
    esac
}

install() {
    echo
	echo -e "\e[29mRecommended hardware requirements.\e[0m" && echo
	echo -e "\e[1;4;29m| 4 CPU | 8 RAM | 200 GB |\e[0m"
	echo 
	echo -e "\e[4;33mDo you want to start the installation?\e[0m" && echo
	echo -e "🟢 | 1.\e[1;32mYes\e[0m"
	echo -e "🔴 | 2.\e[1;31mNo\e[0m"
	echo
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
	case $ans in
		1) 
        sudo apt update && sudo apt upgrade -y
        sudo apt install -y lsof libgomp1
        curl -sSfL 'https://github.com/GaiaNet-AI/gaianet-node/releases/latest/download/install.sh' | bash
        source $HOME/.bashrc
        gaianet init --config https://raw.githubusercontent.com/GaiaNet-AI/node-configs/main/qwen2-0.5b-instruct/config.json
        gaianet start
        mainmenu
         ;;
		2) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/gaianet/main.sh) ;;
		*) clear && echo "Invalid request!" && mainmenu ;;
    esac
}

update() {
    echo 
	echo -e "\e[4;33mDo you want to upgrade node? \e[0m" && echo
	echo -e "🟢 | 1.\e[1;32mYes\e[0m"
	echo -e "🔴 | 2.\e[1;31mNo\e[0m"
	echo
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
	case $ans in
		1) 
        curl -sSfL 'https://github.com/GaiaNet-AI/gaianet-node/releases/latest/download/install.sh' | bash -s -- --upgrade
        mainmenu
         ;;
		2) clear && source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/gaianet/main.sh) ;;
		*) clear && echo "Invalid request!" && mainmenu ;;
    esac
}

delete() {
    echo 
	echo -e "\e[4;33mDo you want to delete node? \e[0m" && echo
	echo -e "🟢 | 1.\e[1;32mYes\e[0m"
	echo -e "🔴 | 2.\e[1;31mNo\e[0m"
	echo
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
	case $ans in
		1)
        curl -sSfL 'https://github.com/GaiaNet-AI/gaianet-node/releases/latest/download/uninstall.sh' | bash
        mainmenu
        ;;
		2) clear && source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/gaianet/main.sh) ;;
		*) clear && echo "Invalid request!" && mainmenu ;;
    esac
}

guide() {
clear && echo -e "\e[35m
╔═╗╦ ╦╦╔╦╗╔═╗  
║ ╦║ ║║ ║║║╣   
╚═╝╚═╝╩═╩╝╚═╝ \e[0m" 
    echo
    echo -ne "\e[3;33mGaiaNet is a decentralized computing infrastructure that enables everyone to create, 
    deploy, scale, and monetize their own AI agents that reflect their styles, values, knowledge, 
    and expertise. It allows individuals and businesses to create AI agents. \e[0m"
    echo && echo
    echo -e "\e[4;32mEnglish guide:\e[0m"
    echo "Medium: Comming soon"
    echo
    echo -e "\e[4;32mRussian guide:\e[0m"
    echo "Boosty: Comming soon"
    echo
    echo -e "\e[4;34mPress Enter to go back to main menu.\e[0m "
    read -r
    source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/gaianet/main.sh)
}

stop() {
    echo
    echo -e "\e[4;33mDo you want to stop node? \e[0m" && echo
    echo -e "🟢 | 1.\e[1;32mYes\e[0m"
    echo -e "🔴 | 2.\e[1;31mNo\e[0m"
    echo
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
        1) gaianet stop && mainmenu ;;
        2) clear && source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/gaianet/main.sh) ;;
        *) clear && echo "Invalid request!" && mainmenu ;;
    esac
}


mainmenu