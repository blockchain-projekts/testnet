#!/bin/bash
clear && echo -e "\e[32m
╦╔╗╔╔═╗╔╦╗╔═╗╦  ╦  ╔═╗╔╦╗╦╔═╗╔╗╔            
║║║║╚═╗ ║ ╠═╣║  ║  ╠═╣ ║ ║║ ║║║║            
╩╝╚╝╚═╝ ╩ ╩ ╩╩═╝╩═╝╩ ╩ ╩ ╩╚═╝╝╚╝ \e[0m"
mainmenu() {
	echo
	echo -e "\e[29mRecommended hardware requirements.\e[0m" && echo
	echo -e "\e[1;4;29m| 4 CPU | 16 RAM | 400 GB |\e[0m"
	echo
	echo -e "\e[4;33mDo you want to start the installation?\e[0m" && echo
	echo -e "1. \e[2;31mYes\e[0m"
	echo -e "2. \e[1;32mNo\e[0m"
	echo
    echo -ne "\e[4;34mYour selection:\e[0m "
    read ans
	case $ans in
		1) yes ;;
		2) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/main.sh) ;;
		*) clear && echo "Invalid request!" && mainmenu ;;
    esac
}

yes(){
	echo
	sudo apt update > /dev/null 2>&1 && sudo apt install curl screen npm -y
	sudo apt install docker.io -y
	docker --version

	sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose
	docker-compose --version
	curl -O https://raw.githubusercontent.com/shardeum/shardeum-validator/refs/heads/itn4/install.sh && chmod +x install.sh && ./install.sh

	docker update --restart always shardeum-validator && docker start shardeum-validator && docker exec -i shardeum-validator /bin/bash -c "operator-cli gui start && operator-cli start"
	docker exec -i shardeum-validator /bin/bash -c "pm2 list"
	screen -ls
	subnmenu

}

subnmenu() {
	echo -ne "
	Installation completed
	Press Enter: "
	read -r ans
	case $ans in
		*) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/main.sh) ;;
    esac
}

mainmenu