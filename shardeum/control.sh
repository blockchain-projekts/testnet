#!/bin/bash
clear && echo -e "\e[34m
╔═╗╔═╗╔╗╔╔╦╗╦═╗╔═╗╦     
║  ║ ║║║║ ║ ╠╦╝║ ║║     
╚═╝╚═╝╝╚╝ ╩ ╩╚═╚═╝╩═╝\e[0m"
mainmenu() {
    echo
	echo " 🪙"
    echo "   | 1.Stake"
    echo "   | 2.Unstake"
    echo "   | 3.Stake Info"
	echo " ⚙️"
	echo "   | 4.Validator status"
    echo "   | 5.pm2 list"
    echo "   | 6.Version info"
	echo "   | 7.Autostart Validator"
	echo "   | 8.Enter Metamask"
	echo "   | 9.Enter Private Key"
	echo "   | 11.Operator GUI start"
	echo "   | 12.Operator CLI start"
    echo "  ----------------"
    echo "  0.Back | 10.Exit"
    echo "  ----------------"
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
   	case $ans in
		1) stake ;;
		2) Unstake ;;
    	3) stakeinfo ;;
		4) status ;;
		5) pm2 ;;
		6) version ;;
		7) autostart ;;
		8) metamask ;;
		9) privkey ;;
		11) guistart ;;
		12) clistart ;;
		0) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/main.sh) ;;
        10) echo "Goodbye." && exit ;;
        *) echo "Invalid request!" && mainmenu ;;
	esac
}


autostart() {

  cd "$HOME"

  if ! command -v screen &> /dev/null; then
    echo && echo "    Installing screen package..." && echo
    sudo apt-get update
    sudo apt-get install screen
  fi

  curl -sS -o autostart_shardeum.sh https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/autostart_shardeum.sh && chmod +x autostart_shardeum.sh
  screen -dmS autostart_shardeum bash autostart_shardeum.sh
  echo && echo "    Autostart validator started in screen!" && echo
  mainmenu
}

stake(){
	echo &&	read -r -p "
$(printCyan 'Enter the amount of SHM:')  " VAR2
	echo
	docker exec -i shardeum-validator /bin/bash -c "export PRIV_KEY="$PRIV_KEY" && operator-cli stake "$VAR2""
	echo
	mainmenu
}

unstake(){
	echo &&	read -r -p "
$(printBYellow 'You can unstake all your tokens by pressing Enter or
		 enter the amount of SHM you want to unstake:')  " VAR1
	echo
	docker exec -i shardeum-validator /bin/bash -c "export PRIV_KEY="$PRIV_KEY" && operator-cli unstake "$VAR1""
	echo
	mainmenu
}
stakeinfo(){
	echo &&	docker exec -i shardeum-validator /bin/bash -c "operator-cli stake_info "$METAMASK"" && echo
	mainmenu
}

status(){
	echo &&	docker exec -i shardeum-validator /bin/bash -c "operator-cli status" && echo
	mainmenu
}

pm2(){
	echo &&	docker exec -i shardeum-validator /bin/bash -c "pm2 ls" && echo
	mainmenu
}

version(){
	echo &&	docker exec -i shardeum-validator /bin/bash -c "operator-cli version" && echo
	mainmenu
}

privkey(){
	echo -ne "
$(printCyan 'Paste your private key Metamask') "
	read -r PRIV_KEY
	echo "export PRIV_KEY="$PRIV_KEY"" >> ~/.bash_profile && source ~/.bash_profile
	echo
	mainmenu
}

metamask(){
	echo -ne "
$(printCyan 'Paste your Metamask address') "
	read -r METAMASK
	echo "export METAMASK="$METAMASK"" >> ~/.bash_profile && source ~/.bash_profile
	echo
	mainmenu
}

guistart(){
	echo &&	docker exec -i shardeum-validator /bin/bash -c "operator-cli gui start" && echo
	mainmenu
}

clistart(){
	echo &&	docker exec -i shardeum-validator /bin/bash -c "operator-cli start" && echo
	mainmenu
}

mainmenu