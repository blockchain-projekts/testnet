#!/bin/bash
clear && echo -e "\e[31m
╔╦╗╔═╗╦  ╔═╗╔╦╗╔═╗                          
 ║║║╣ ║  ║╣  ║ ║╣                           
═╩╝╚═╝╩═╝╚═╝ ╩ ╚═╝\e[0m" && echo
mainmenu() {
	echo -e "\e[4;33mYou sure you want to delete Shardeum?\e[0m" && echo
	echo -e "1. \e[2;31mYes\e[0m"
	echo -e "2. \e[1;32mNo\e[0m"
	echo
    echo -ne "\e[4;34mYour selection:\e[0m "
    read ans
	case $ans in
		1) yes ;;
		2) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/main.sh) ;;
		*) echo "Invalid request!" && mainmenu
		;;
	esac
}

yes(){
	echo "\e[4;33mDeleting...\e[0m"
	cd ~/.shardeum && ./cleanup.sh && cd ~/ && rm -rf .shardeum
	rm installer.sh
	submenu
}

submenu(){
    echo -e "\e[32m
┌─┐┌─┐┌┬┐┌─┐┬  ┌─┐┌┬┐┌─┐┌┬┐                 
│  │ ││││├─┘│  ├┤  │ ├┤  ││                 
└─┘└─┘┴ ┴┴  ┴─┘└─┘ ┴ └─┘─┴┘\e[0m" 
echo -e "\e[2;32mShardeum deleted successfully!\e[0m"
echo
echo -n "Press Enter:  "
	read -r ans
	case $ans in
		*) clear && source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/main.sh) ;;
	esac
}
mainmenu