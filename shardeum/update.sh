#! /bin/bash
#!/bin/bash
clear && echo -e "\e[33m
╦ ╦╔═╗╔╦╗╔═╗╔╦╗╔═╗
║ ║╠═╝ ║║╠═╣ ║ ║╣ 
╚═╝╩  ═╩╝╩ ╩ ╩ ╚═╝ \e[0m"
mainmenu() {

	echo
	echo -e "\e[4;33mDo you want to start the update?\e[0m" && echo
	echo -e "1. \e[2;31mYes\e[0m"
	echo -e "2. \e[1;32mNo\e[0m"
	echo
    echo -ne "\e[4;34mYour selection:\e[0m "
    read ans
	case $ans in
		1) yes ;;
		2) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/main.sh) ;;
		*) clear && echo "Invalid request!" && mainmenu ;;
    esac
}


yes(){
sudo apt-get update && sudo apt-get upgrade -y
cd $HOME && docker exec -i shardeum-dashboard /bin/bash -c "operator-cli stop" 
curl -O https://raw.githubusercontent.com/shardeum/shardeum-validator/refs/heads/itn4/install.sh && chmod +x install.sh && ./install.sh
docker exec -i shardeum-validator /bin/bash -c "operator-cli gui start && operator-cli start"
docker exec -i shardeum-validator /bin/bash -c "pm2 list"
complete_installation
}


complete_installation() {
	echo -e "\e[5;1;32mUpdate complete! \e[0m"

    echo
    echo -ne "\e[4;34mPress Enter to go main menu. \e[0m"
    read -r ans
    case $ans in
        *) cd && source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/main.sh) ;;
    esac
}
mainmenu
