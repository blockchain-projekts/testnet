#!/bin/bash
clear && echo -e "\e[35m
╔═╗╦ ╦╔═╗╦═╗╔╦╗╔═╗╦ ╦╔╦╗                    
╚═╗╠═╣╠═╣╠╦╝ ║║║╣ ║ ║║║║                    
╚═╝╩ ╩╩ ╩╩╚══╩╝╚═╝╚═╝╩ ╩\e[0m"
mainmenu() {
    echo
    echo " 🎛️  | 1.Control"
    echo " 🔄 | 2.Update CLI"
    echo " 💿 | 3.Install"
    echo " 🗑️  | 4.Delete"
    echo "  ----------------"
    echo "  0.Back | 10.Exit"
    echo "  ----------------"
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
        1) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/control.sh) ;;
        2) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/update.sh) ;;
        3) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/install.sh) ;;
        4) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/delete.sh) ;;
        0) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/testnet.sh) ;;
        10) echo "Goodbye." && exit ;;
        *) echo -e "\e[31mInvalid request!\e[0m" && mainmenu ;;
    esac
}
mainmenu